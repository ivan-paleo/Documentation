---
eleventyNavigation:
  key: CodebergPages
  title: Codeberg Pages
  icon: server
  order: 60
---

Codeberg allows you to publish static web content (HTML, images, etc) with a human-friendly address ({user-name}.codeberg.page).


1. Create a repository named 'pages' in your user account or organization.
2. Create static content, HTML, style, fonts or images. Name the homepage file 'index.html'
3. Push your content to the main branch of the new repository.
4. You should now be able to access your content using the domain '{user-name}.codeberg.page'.

See also [https://codeberg.page/](https://codeberg.page)
