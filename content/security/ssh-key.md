---
eleventyNavigation:
  key: SSHKey
  title: Adding an SSH key to your account
  parent: Security
---

It is recommended to use one key per client. It means if you access your Codeberg repository from your home PC, your laptop and your office PC you should generate separate keys for each machine.

In the context of Codeberg you should decide yourself if you add a pasphrase to your SSH key or not. Read the on-going discussion about that topic in this [Codeberg-Issue](https://codeberg.org/Codeberg/Documentation/issues/2).

## Generating a SSH key (pair)

1. Open a shell (e.g. `git-bash` on Windows or `bash` on Linux).

2. Paste the text below, substituting in your Codeberg email address.

    ```shell
    $ ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
    ```

    This creates a new ssh key, using the provided email as a label:

    ```shell
    > Generating public/private rsa key pair.
    ```

3. When you're prompted to "Enter a file in which to save the key," press <kbd>Enter</kbd>. This accepts the default file location:

    ```shell
    > Enter a file in which to save the key (/c/Users/you/.ssh/id_rsa):[Press enter]
    ```

4. Enter a passphrase if you want. *See further [discussions](https://codeberg.org/Codeberg/Documentation/issues/2) about the necessity of a passphrase in the context of Codeberg.*

## Add the SSH key to Codeberg
1. Copy the SSH key to your clipboard. Attention: Copy only the public part of the key not the private one. You can identify it by the `.pub` extension. By default you can find the public key in `$HOME/.ssh/id_rsa.pub`.

    On Linux you can use the `xclip` command like this
    ```shell
    $ xclip -sel clip < ~/.ssh/id_rsa.pub
    ```
    
    On Windows use a text editor (e.g. Notepad) or `clip` on the commandline
    ```shell
    $ type .ssh/id_rsa.pub | clip
    ```
2. Navigate to your user settings
<picture>
  <source srcset="/assets/images/security/user-settings.webp" type="image/webp">
  <img src="/assets/images/security/user-settings.png" alt="User Settings">
</picture>

3. Go to the settings section __SSH / GPG Keys__ and click on __Add key__.

<picture>
  <source srcset="/assets/images/security/ssh-key/add-ssh-key.webp" type="image/webp">
  <img src="/assets/images/security/ssh-key/add-ssh-key.png" alt="SSH Key Settings">
</picture>
 
4. Give an appropriate name for the key.
5. Paste your key string from the clipboard into __content__ field.

## Test the SSH connection
Do this simple test:

```shell
$ ssh -T git@codeberg.org
```

The output should look like this:

```shell
Hi there, You've successfully authenticated, but Gitea does not provide shell access.
If this is unexpected, please log in with password and setup Gitea under another user.
```
    
*Note: All codeberg users share a single unix user named `git` which is used to check out repositories. Depending on the key provided, permission is granted or denied. You can check out all repositories with your key which you have permission for. You can push code to all repositories where you have write access.*

## Avoid re-typing the passphrase

Assuming you created a secure key with passphrase, SSH will prompt you for your passphrase for every connection. Common desktop environments like OSX or Gnome will offer you to cache your passphrase via an SSH agent.

If you are working at the command line, you can alternatively do this directly:

```shell
$ eval `ssh-agent`
$ ssh-add
## enter your passphrase once, this is then cached.
```